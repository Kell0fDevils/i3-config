My i3wm Config requires:<br>
i3-status

My autostart requires:<br>
feh with Xinerama support enabled (use "man feh" to check)<br>
picom<br>
volumeicon applet<br>
lxqt-policykit-agent<br>
lxqt-notificationd<br>

Besides the little documentation above. I won't provide further<br>
documentation. It's up the user (you) to make changes to what they<br>
(you) don't want.<br>

Goto to the [i3wm User's guide](https://i3wm.org/docs/userguide.html) for further documentation.
