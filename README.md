My i3wm Config requires:<br>
bumblebee-status (Not Arch Linux) <br>
i3status or i3status-rust (Arch Linux only) <br>

My autostart requires:<br>
feh with Xinerama support enabled (use "man feh" to check)<br>
picom<br>
volumeicon applet<br>
lxqt-policykit-agent<br>

Besides the little documentation above. I won't provide further<br>
documentation. It's up the user (you) to make changes to what they<br>
(you) don't want.<br>

Goto to the [i3wm User's guide](https://i3wm.org/docs/userguide.html) for further documentation.
